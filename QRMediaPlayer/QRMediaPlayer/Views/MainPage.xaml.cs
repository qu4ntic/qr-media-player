﻿using LibVLCSharp.Forms.Shared;
using LibVLCSharp.Shared;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using QRMediaPlayer.Helpers;
using QRMediaPlayer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;


namespace QRMediaPlayer.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        private ZXingScannerView zxing;
        AssetDatabase db = AssetDatabase.Db;
        Button clearButton;

        LibVLC libVLC;
        MediaPlayer mediaPlayer;

        bool hasBeenCreated = false;

        bool soundIsPlaying = false;
        bool SoundIsPlaying
        {
            get {
                return soundIsPlaying;
            }

            set
            {
                soundIsPlaying = value;
                if (!value)
                {
                    MainThread.BeginInvokeOnMainThread(() =>
                    {
                        setClearButtonCount();
                    });
                    
                }
                MainThread.BeginInvokeOnMainThread(() =>
                {
                    clearButton.IsEnabled = !soundIsPlaying;
                });

            }
        }

        public MainPage() : base()
        {
            if (!hasBeenCreated)
            {
                hasBeenCreated = true;
                // Initialize ZXingScanner view
                zxing = new ZXingScannerView
                {
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand
                };
                // Execute some code When the camera detect a QR code
                zxing.OnScanResult += (result) =>
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                    // Stop analyzing
                    zxing.IsAnalyzing = false;
                        string key = result.Text;

                    // Check if key is known in the database
                    Asset asset = db.GetAssetAsync(key).Result;
                        bool isNewQrCode = asset == null;

                    // If QR code is unknown, upload and associate a new media file
                    if (isNewQrCode)
                        {
                            await DisplayAlert("Nouveau QR code", "Veuillez sélectionner un media à associer à `" + key + "`", "OK");
                            string filename = await PickFile();
                            if (filename.Length > 0)
                            {
                                Associate(key, filename);
                                // Let's scan again
                                zxing.IsAnalyzing = true;
                            }
                        }
                        else

                    // QR code has already been uploaded and we should play the media
                    {
                            clearButton.Text = "Le son `" + asset.Id + "` est joué!";
                            SoundIsPlaying = true;

                            mediaPlayer.Play(new Media(libVLC, asset.MediaPath, FromType.FromPath));
                        }
                    });

                // Create clear button
                clearButton = new Button();
                setClearButtonCount();
                clearButton.HorizontalOptions = LayoutOptions.FillAndExpand;
                clearButton.VerticalOptions = LayoutOptions.End;
                clearButton.BackgroundColor = Color.Chartreuse;
                clearButton.Clicked += OnButtonClicked;

                // Load player
                Core.Initialize();
                libVLC = new LibVLC();
                mediaPlayer = new MediaPlayer(libVLC);
                mediaPlayer.EndReached += (sender, e) =>
                {
                    SoundIsPlaying = false;
                    zxing.IsAnalyzing = true;
                };

                // Create grid
                var grid = new Grid
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };

                // Adds ZXing in the grid
                grid.Children.Add(zxing);
                grid.Children.Add(clearButton);

                Content = grid;
            }
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            // Scan
            zxing.IsScanning = true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            
            // Stop scanning
            zxing.IsScanning = false;
        }


        private async Task<string> PickFile()
        {
            try
            {
                FileData fileData = await CrossFilePicker.Current.PickFile();
                if (fileData == null)
                    return ""; // user canceled file picking

                string fileName = fileData.FilePath;

                return fileName;
            }
            catch
            {
                return ""; // return no file but don't crash please!
            }
        }



        private async void Associate(string key, string filename)
        {


            IFileHelper fileHelper = DependencyService.Get<IFileHelper>();

            bool needsContentResolver = filename.StartsWith("content://");

            string resolvedFilename = "";
            if (needsContentResolver)
            {
                resolvedFilename = fileHelper.GetPathFromUri(filename);
            } else
            {
                resolvedFilename = Path.Combine(Constants.FilePath, Path.GetFileName(filename));
                File.Copy(filename, resolvedFilename);
            }

            
            await db.SaveAssetAsync(new Asset
            {
                Id = key,
                MediaPath = resolvedFilename
            });
            setClearButtonCount();
        }

       


        async void OnButtonClicked(object sender, EventArgs args)
        {

            foreach(Asset asset in db.GetAssetsAsync().Result)
            {
                if (asset.MediaPath != null)
                    try
                    {
                        File.Delete(asset.MediaPath);
                    } catch
                    {
                        await DisplayAlert("Erreur", "Suppression de `" + asset.MediaPath + "` impossible!", "OK");
                    }
            }

            await db.DeleteAllAssetsAsync();
            setClearButtonCount();
        }

        void setClearButtonCount()
        {
            int numAssets = db.CountAssetsAsync().Result;

            if (numAssets == 0)
            {
                clearButton.Text = "Associez votre 1er QR code!";
                clearButton.IsEnabled = false;
            }
            else
            {
                clearButton.Text = "Vider les " + numAssets + " associations";
                clearButton.IsEnabled = true;
            }

        }

    }
}