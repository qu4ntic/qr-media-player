﻿
using QRMediaPlayer.Config;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QRMediaPlayer.Models
{
    public class Asset
    {
        // Stores the content as a key to find the associated media
        public string Id
        {
            get;
            set;
        }
        // Stores a local path to the media
        public string MediaPath
        {
            get; 
            set;
        }
    }


    public class AssetDatabase
    {

        static AssetDatabase db;
        public static AssetDatabase Db
        {
            get
            {
                if (db == null)
                {
                    db = new AssetDatabase();
                }
                return db;
            }

        }




        static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public AssetDatabase()
        {
            InitializeAsync().SafeFireAndForget(false);
        }

        async Task InitializeAsync()
        {
            if (!initialized)
            {
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(Asset).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Asset)).ConfigureAwait(false);
                    initialized = true;
                }
            }
        }

        public Task<List<Asset>> GetAssetsAsync()
        {
            return Database.Table<Asset>().ToListAsync();
        }

        public Task<List<Asset>> GetAssetsNotDoneAsync()
        {
            // SQL queries are also possible
            return Database.QueryAsync<Asset>("SELECT * FROM [Asset] WHERE [Done] = 0");
        }

        public Task<Asset> GetAssetAsync(string id)
        {
            return Database.Table<Asset>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<int> CountAssetsAsync()
        {
            return Database.Table<Asset>().CountAsync();
        }

        public Task<int> SaveAssetAsync(Asset asset)
        {
            if (GetAssetAsync(asset.Id).Result == null)
            {
                return Database.InsertAsync(asset);
            } else
            {
                return Database.UpdateAsync(asset);
            }
        }

        public Task<int> DeleteAllAssetsAsync()
        {
            return Database.DeleteAllAsync(Database.TableMappings.First());
        }


    }

}