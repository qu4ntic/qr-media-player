﻿
namespace QRMediaPlayer.Helpers
{
    public interface IFileHelper
    {

        string GetPathFromUri(string uriString);
    }
}